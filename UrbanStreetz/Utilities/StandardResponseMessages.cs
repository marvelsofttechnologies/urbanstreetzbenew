﻿using System;
namespace UrbanStreetz.Utilities
{
    public class StandardResponseMessages
    {
        public StandardResponseMessages()
        {
        }
        public static string OK => SUCCESSFUL;
        public static string PASSWORD_RESET_EMAIL_SENT = "An email has been sent to you with instructions and next steps";
        public static string SUCCESSFUL = "Successful";
        public static string USER_NOT_PERMITTED = "Sorry you are not permitted to log in as an administrator";
        public static string UNSUCCESSFUL = "Unsuccessful";
        public static string ERROR_OCCURRED = "An Error Occurred, please try again later";
    }
}

