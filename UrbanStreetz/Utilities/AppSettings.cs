﻿using System;
namespace UrbanStreetz.Utilities
{
    public class AppSettings
    {

        public string Secret { get; set; }
        public string ConnectionString { get; set; }

        public string SendGridApiKey { get; set; }

        public string SenderEmail { get; set; }

        public string FrontEndBaseUrl { get; set; }
        public string EmailVerificationUrl { get; set; }
        public string PasswordResetUrl { get; set; }
        public string GoogleCredentialsFIle { get; set; }
        public string GoogleStorageBucket { get; set; }
        public string UploadDrive { get; set; }
        public string DriveName { get; set; }
        public string SendersName { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public string LogoURL { get; set; }
        public string MailGunBaseUrl { get; set; }
        public string MailGunApiKey { get; set; }
    }

    public class UserType
    {
        public static readonly string SUPER_ADMIN = "SUPER ADMIN";
        public static readonly string ADMIN = "ADMIN";
        public static readonly string USER = "USER";
    }
}

